import argparse

from .game.engine import Engine


def get_args(argv=None) -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Let's play almost lotto")
    parser.add_argument('-s', '--simple-console',
                        action='store_false',
                        help="use a simple console without colors")
    parser.add_argument('-c', '--clear-console',
                        action='store_true',
                        help="clear the console with every new number")
    parser.add_argument('-S', '--skip-bots',
                        action='store_true',
                        help="skip output of bots")

    return parser.parse_args(argv)


def init_engine(args) -> Engine:
    return Engine(args.clear_console, args.simple_console, args.skip_bots)


def main():
    try:
        engine = init_engine(get_args())
        engine.main_menu()
    except KeyboardInterrupt:
        print('Exit with KeyboardInterrupt')


if __name__ == '__main__':
    main()
