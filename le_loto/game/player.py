import random
import time
from abc import abstractmethod
from dataclasses import dataclass
from distutils.util import strtobool
from enum import Enum

from .card import Card

PLAYERS_NAME = ['Mr. Orange', 'Mr. Blonde', 'Mr. Pink', 'Mr. Blue', 'Mr. Brown', 'Mr. White']


@dataclass
class Stat:
    """Storing win/lose statistics"""
    win: int = 0
    lose: int = 0

    def percentage_of_wins(self) -> float:
        if not self.win:
            return 0.0
        if not self.lose:
            return 100.0
        return self.win / (self.win + self.lose) * 100


class BotDifficulty(Enum):
    EASY = 0
    NORMAL = 1
    HARD = 2


class Player:
    _is_bot: bool = False

    def __init__(self, name: str, card: Card):
        self._name: str = name
        self._card: Card = card
        self.__check_card()
        self.stat: Stat = Stat()

    def is_num_in_card(self, num: int) -> bool:
        """
        Checks if there is a number in the card and crosses it out, returns True. Else False
        """
        if num in self.card:
            self.card.cross_out(num)
            return True
        return False

    def new_card(self, card: Card = None):
        """
        Creates a new card, or uses the passed argument
        """
        if not card:
            self.card.create()
        else:
            self._card = card

    def __check_card(self):
        if len(self.card) == 0:
            self.card.create()

    @property
    def card(self):
        return self._card

    def win(self):
        """
        increase the statistics of wins
        """
        self.stat.win += 1

    def lose(self):
        """
        increase statistics of losses
        """
        self.stat.lose += 1

    @abstractmethod
    def cross_out_current_num(self, num: int):
        raise NotImplementedError

    @property
    def name(self):
        return '({}) {}'.format(type(self).__name__, self._name)

    @property
    def is_bot(self) -> bool:
        return self._is_bot

    def __str__(self):
        return '{}\n|{:^26}|\n{}'.format('_' * 28, self.name, self.card)


class Bot(Player):
    _is_bot: bool = True

    def __init__(self, name: str, card: Card, difficulty: BotDifficulty = BotDifficulty.NORMAL):
        super().__init__(name, card)
        self.difficulty: BotDifficulty = difficulty

    def cross_out_current_num(self, num: int) -> bool:
        """
        The robot crosses out the number depending on the difficulty.
        Mistakes probability: easy 1/5, normal 1/50, hard never make.
        """
        print('Cross out the num? y/n')
        time.sleep(random.uniform(0.0, 0.5))
        cross_out = self.making_choice(num)
        return cross_out

    def making_choice(self, num: int) -> bool:
        num_in_card = num in self.card
        cross_out = num_in_card
        if self.difficulty is BotDifficulty.EASY:
            cross_out = random.choices([num_in_card, not num_in_card], [5, 1])[0]
        elif self.difficulty is BotDifficulty.NORMAL:
            cross_out = random.choices([num_in_card, not num_in_card], [50, 1])[0]
        return cross_out


class Human(Player):
    def cross_out_current_num(self, num: int) -> bool:
        """
        Ask the human if he crosses out the number.
        strtobool from users input.
        """
        try:
            return bool(strtobool(input('Cross out the num? y/n\n').lower()))
        except ValueError:
            return False


if __name__ == '__main__':
    random.shuffle(PLAYERS_NAME)
    player1 = Bot(PLAYERS_NAME.pop(), Card(), BotDifficulty.NORMAL)
    player2 = Bot(PLAYERS_NAME.pop(), Card(), BotDifficulty.HARD)
    print(player1)
    print(player2)
    print('*' * 36)

    for j in range(1, 91, 3):
        print(j)
        if player1.cross_out_current_num(j):
            player1.is_num_in_card(j)
        # if player2.cross_out_current_num(j):
        #     player2.is_num_in_card(j)
        print(player1)
        # print(player2)
        print('*' * 48)
        # if player1.is_num_in_card(j):
        #     print('{}: {} in card'.format(player1.name, j))
        # if player2.is_num_in_card(j):
        #     print('{}: {} in card'.format(player2.name, j))
    print(player1)
    print(player2)
