import random
from collections import defaultdict
from enum import Enum
from typing import Dict


class CardType(Enum):
    LOTTO = 1
    RANDOM = 2
    INCREASING_RANDOM = 3


class Card:
    def __init__(self, card_type: CardType = CardType.LOTTO):
        """
        :param card_type: type of card to use the create method (default: CardType.LOTTO)
        :type card_type: CardType
        """
        self.card: list = list()
        self.card_type: CardType = card_type

        self.__num_coordinates: Dict[int, tuple] = defaultdict(tuple)

    def create(self):
        """
        Creating a card depending on the card_type parameter
        """
        if self.card_type is CardType.RANDOM:
            self.create_random()
        elif self.card_type is CardType.INCREASING_RANDOM:
            self.create_increasing_random()
        else:
            self.create_lotto()

    def create_lotto(self):
        """
        Creating a standard lotto card, where the first column is 1-9, the second is 10-19 and so on, the last is 80-90
        ----------------------------
        | 4    24 39       61    86|
        | 6    23    48 53 65      |
        | 9          42    62 79 85|
        ----------------------------
        """
        self.card_type = CardType.LOTTO
        cols = [list(range(1, 10)), *[list(range(x, x + 10)) for x in range(10, 80, 10)], list(range(80, 91))]
        self._create_card(cols)
        self._update_num_coordinates()

    def create_random(self):
        """
        Creating a card with random numbers in each column
        ----------------------------
        |46 69    38    84 11      |
        |34 16  3 65       31      |
        |   90  5          83 15  7|
        ----------------------------
        """
        self.card_type = CardType.RANDOM
        raw_cols = list(range(1, 91))
        random.shuffle(raw_cols)
        cols = [raw_cols[x:x + 10] for x in range(0, len(raw_cols), 10)]
        self._create_card(cols)
        self._update_num_coordinates()

    def create_increasing_random(self):
        """
        Creating a card with random numbers sorted in ascending order ->
        ----------------------------
        |16 19 20 50    53         |
        |21       27 61    76 84   |
        |17 55    59 68          87|
        ----------------------------
        """
        self.create_random()
        self.card_type = CardType.INCREASING_RANDOM
        for i in range(len(self.card)):
            sort_row = sorted((x for x in self.card[i] if isinstance(x, int)))
            line = [sort_row.pop(0) if isinstance(x, int) and sort_row else x for x in self.card[i]]
            self.card[i] = line
        self._update_num_coordinates()

    def _create_card(self, cols: list):
        self.card.clear()

        for i in range(3):
            filling_cols = random.sample(range(9), k=5)
            line = [cols[x].pop(random.randint(0, len(cols[x]) - 1)) if x in filling_cols else ' ' for x in range(9)]
            self.card.append(line)

    def _update_num_coordinates(self):
        """
        To quickly find a number in a card, we use a dictionary with num coordinates
        {num: (line, column)}
        """
        self.__num_coordinates.clear()
        for i in range(len(self.card)):
            line = self.card[i]
            self.__num_coordinates.update({line[x]: (i, x) for x in range(len(line)) if isinstance(line[x], int)})

    def cross_out(self, num: int) -> bool:
        if num in self:
            line, col = self.__num_coordinates.pop(num)
            self.card[line][col] = '--'
            return True
        return False

    def __str__(self):
        line_as_string = ['|{:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2}|'.format(*x) for x in self.card]
        card_as_string = '\n'.join(line_as_string)
        return "{}\n{}\n{}".format('-' * 28, card_as_string, '-' * 28)

    def __contains__(self, item):
        return item in self.__num_coordinates

    def __len__(self):
        return len(self.__num_coordinates)


if __name__ == '__main__':
    card_ = Card()
    card_.create_random()
    print(card_)
    print('*' * 36)
    card_.create_increasing_random()
    print(card_)
    for j in range(30, 40):
        print('{} in card: {}'.format(j, j in card_))
        card_.cross_out(j)
    print(card_)
