from unittest import mock

import pytest

from le_loto.game.engine import Engine


@pytest.fixture
def engine():
    engine = Engine()
    yield engine


class TestEngine:
    def test_init(self):
        test_engine = Engine(with_console_clear=True, with_color=True, skip_bots=True)
        assert test_engine.with_console_clear is True
        assert test_engine.with_colorama is True
        assert test_engine.skip_bots is True

    def test_append_players(self, engine):
        with mock.patch('builtins.input', return_value='1'):
            engine.append_players()
            assert len(engine._players) == 4
