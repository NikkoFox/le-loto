from unittest import mock

import pytest

from le_loto.game.card import Card
from le_loto.game.player import Player, Stat, Human, Bot, BotDifficulty


@pytest.fixture
def player():
    player = Player('TestPlayer', Card())
    yield player


class TestPlayer:
    def test_init(self):
        test_name = 'Butch'
        test_card = Card()
        player = Player(test_name, test_card)
        assert player.name == '(Player) {}'.format(test_name)
        assert player.card == test_card
        assert player.is_bot is False
        assert str(player).endswith(str(test_card))
        assert isinstance(player.stat, Stat)

    def test_stat(self, player):
        assert player.stat.win == 0 and player.stat.lose == 0
        assert player.stat.percentage_of_wins() == float(0)
        player.win()
        assert player.stat.win == 1 and player.stat.lose == 0
        assert player.stat.percentage_of_wins() == float(100)
        player.lose()
        assert player.stat.win == 1 and player.stat.lose == 1
        assert player.stat.percentage_of_wins() == float(50)

    def test_create_new_card(self, player):
        old_card = str(player.card)
        player.new_card()
        assert str(old_card) != str(player.card)
        test_card = Card()
        player.new_card(test_card)
        assert test_card != player.card

    def test_num_in_card(self, player):
        num = next(x for x in player.card.card[0] if isinstance(x, int))
        assert player.is_num_in_card(num) is True
        assert player.is_num_in_card(999) is False


@pytest.fixture
def human():
    human = Human('TestPlayer', Card())
    yield human


class TestHuman:
    def test_init(self):
        test_name = 'Butch'
        test_card = Card()
        human = Human(test_name, test_card)
        assert human.name == '(Human) {}'.format(test_name)
        assert human.card == test_card
        assert human.is_bot is False
        assert str(human).endswith(str(test_card))
        assert isinstance(human.stat, Stat)

    def test_cross_out_current_num(self, human):
        num = next(x for x in human.card.card[0] if isinstance(x, int))
        for yes_test_input in ('Y', 'y', 'yes'):
            with mock.patch('builtins.input', return_value=yes_test_input):
                human_choice = human.cross_out_current_num(num)
                assert input() == yes_test_input
                assert human_choice is True
        for no_test_input in ('N', 'n', 'no'):
            with mock.patch('builtins.input', return_value=no_test_input):
                human_choice = human.cross_out_current_num(num)
                assert input() == no_test_input
                assert human_choice is False
        for incorrect_test_input in ('100', 'foo', ' n e t '):
            with mock.patch('builtins.input', return_value=incorrect_test_input):
                human_choice = human.cross_out_current_num(num)
                assert input() == incorrect_test_input
                assert human_choice is False


@pytest.fixture(params=[0, 1, 2])
def bot_difficulty(request):
    yield BotDifficulty(request.param)


class TestBot:
    def test_init(self, bot_difficulty):
        test_name = 'Decepticon'
        test_card = Card()
        bot = Bot(test_name, test_card, bot_difficulty)
        assert bot.name == '(Bot) {}'.format(test_name)
        assert bot.card == test_card
        assert bot.is_bot is True
        assert str(bot).endswith(str(test_card))
        assert isinstance(bot.stat, Stat)
        assert bot.difficulty is BotDifficulty(bot_difficulty.value)

    def test_cross_out_current_num_hard_bot(self):
        bot = Bot('Decepticon', Card(), BotDifficulty.HARD)
        num = next(x for x in bot.card.card[0] if isinstance(x, int))
        if bot_difficulty is BotDifficulty.HARD:
            assert bot.cross_out_current_num(num) is True
            assert bot.cross_out_current_num(999) is False
