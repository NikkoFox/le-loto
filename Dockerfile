FROM python:3.7-alpine

WORKDIR .

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY le_loto le_loto

CMD ["python", "-m", "le_loto"]